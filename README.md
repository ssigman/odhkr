# README #

This repository contains the design and code base for the ODHkr (Ozarks Day Hiker) mobile application.  The application (including its design) has been developed to support CSCI 371: Software Engineering at Drury University.  You are welcome to use explore the project or to extend it.

### Organization ###
The repository is organized around two folders.  The Design folder contains design documents and the CodeBase folder contains the code for the Apache Cordova project.  Note: The code has only been tested on the Android platform, but should deploy to iOS also.
 
The project illustrates development following the Pedal Software Development Process as defined by C. Browning and S. Sigman of Drury University.  The master branch contains the Sprint 1 release design and code for the project.  The Sprint 1 branch contains the project work for the first Sprint of the Project and will be identical to the master branch after 12/1/2016.

### How do I get set up? ###

* Download and install Apache Cordova.  You can find tutorials on how to do so.
* Clone the repository.  
* Create a Cordova project linked to the ODHKr-www folder at <repository-root>/CodeBase/ODHKr-www.
* Add the Cordova platforms you want to deploy on.  Tested Platform is Android.
* Add the following Cordova platforms: Device, Notification, Whitelist.
* Obtain a Google Maps API key and replace the key in the code at  <repository-root>/CodeBase/ODHKr-www/index.html with your key.
* Follow the directions to from the Cordova tutorial for deploying the application on an emulator or a device.

### Additional Information ###

Contact the repository owner.