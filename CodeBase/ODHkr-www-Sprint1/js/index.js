/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

var app = {
    // Application Constructor
    initialize: function() {
        console.log("Initializing app");
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
        $(document).on('pageshow', '#map_page', this.onPageShow);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function() {
//        app.receivedEvent('deviceready');
        console.log("deviceready event has fired!");
    },
    onPageShow: function(e, data) {
        $('#content').height(getRealContentHeight());

        // This is the minimum zoom level that we'll allow
        var minZoomLevel = 17;

        var map = new google.maps.Map(document.getElementById('mapCanvas'), {
          zoom: minZoomLevel,
          center: new google.maps.LatLng(37.218937, -93.285804),
          mapTypeId: google.maps.MapTypeId.ROADMAP
       }); 
       console.log("map loaded");
    },
    
    // Update DOM on a Received Event
   /* receivedEvent: function(id) {
        //Log the event start
        console.log("Device is ready");
        
        //Make the device display string
        var tmpStr = this.makeListItem('Cordova Version: ' + device.cordova);
        tmpStr += this.makeListItem('Operating System: ' + device.platform);
        tmpStr += this.makeListItem('OS Version: ' + device.version);
        tmpStr += this.makeListItem('Device Model: ' + device.model);
        tmpStr += this.makeListItem('UUId: ' + device.uuid);
        
        //Write the values to the unordered list
        $('#devInfo').html(tmpStr);
        $('#devInfo').listview('refresh');

    },*/
            
 /*   makeListItem: function (textStr){
        return '<li>' + textStr + '</li>';
    }*/
};

function getRealContentHeight() {
	var header = $.mobile.activePage.find("div[data-role='header']:visible");
	var footer = $.mobile.activePage.find("div[data-role='footer']:visible");
	var content = $.mobile.activePage.find("div[data-role='content']:visible:visible");
	var viewport_height = $(window).height();

	var content_height = viewport_height - header.outerHeight() - footer.outerHeight();
	if((content.outerHeight() - header.outerHeight() - footer.outerHeight()) <= viewport_height) {
		content_height -= (content.outerHeight() - content.height());
	} 
	return content_height;
};

/*function hikeMap() {
    this.map=null;
    console.log("hikeMap constructor called");
}

hikeMap.prototype.initMap = function() {
    console.log("hikeMap initMap function called");
    this.map = new google.maps.Map(document.getElementById('mapCanvas'), {
            center: {lat: 37.218937, lng: -93.285804},
            zoom: 17
    });
    alert("Map: " + this.map);
    console.log("hikeMap initMap function exiting");
};*/

app.initialize();